# somedcc

A fake DCC app with feature you find in classic qt-based DCCs like Maya, Nuke, etc...

This is handy while designing pipeline systems / PoC / Prototypes and need to easily simulate dependencies, environments, etc...

The app will let you manage a tree of items.

Available/Planned features:
- Items 
    - ☑ load project
    - ☑ save project
    - ☑ create items
    - ☑ get/set item properties
    - ☐ delete items
    - ☐ select items
    - ☑ link item
    - ~ include projects  
    - ☐ import project
    - ☐ export item
- Command API
    - ☐ everything the GUI can do.
    - ☐ declare new item types.
    - ☐ add some GUI components (menu, menu items, toolbars, buttons).
- SDK
    - ☐ Install custom commands / command groups
    - ☐ Override existing commands
    - ☐ pub/sub to sync views and models
    - ☐ Declare GUI panels
    - ☐ Define a whole new app
- Env vars
    - SOMEDCC_STARTUP_SCRIPT: 
        ☑ Path to a python file to execute at app startup.
    - SOMEDCC_SCRIPTS_PATH: 
        ☑ List of paths to look up for a `startup.py` file and execute it at app startup.
        ☑ Each path is aslo added to `sys.path`
- Headless mode to execute scripts.


# Install
In your virtual env:
    `pip install somedcc`

# Launch
In you virtual env:

`someddcc`

From outside your virtual env:

`/path/to/my/venv/bin/somedcc`

Launch and open a project:

`somedcc project.sd`

Launch headless and execute a script:

`somedccpy my_script.py`

    Note: `somedcc` vs `somedccpy`, gui vs headless.


# Configure

## Startup Script
Set the env var `SOMEDCC_STARTUP` to a python file, it will be executed on startup.

## Multiple Startup Scripts

Set the env var `SOMEDCC_STARTUP_PATH` to a semicolon separated list of folder. Each folder will be scanned for a `some_startup.py` file to execute.

    Note: the execution order will follow the order of the paths in the env var.

