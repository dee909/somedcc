import pytest
import os
from pathlib import Path

from somedcc.app.gui import SomeDCCGUI
from somedcc.sdk.app import StartupScriptError


def test_startup_script_doesnt_exists():
    os.environ["SOMEDCC_STARTUP_SCRIPT"] = "blah"
    os.environ.pop("SOMEDCC_SCRIPTS_PATH", None)

    with pytest.raises(StartupScriptError):
        SomeDCCGUI()


def test_startup_script(tmp_path):
    script_content = """\
somedcc.cmds.app.notify("This is my startup !!!")

somedcc.set_data("TEST", "YEAH!")
"""
    script_path = tmp_path / "startup_script.py"
    with open(script_path, "w") as fp:
        fp.write(script_content)

    os.environ["SOMEDCC_STARTUP_SCRIPT"] = str(script_path)
    os.environ.pop("SOMEDCC_SCRIPTS_PATH", None)

    somedcc = SomeDCCGUI()

    assert somedcc.get_data("TEST") == "YEAH!"


def test_startup_script_error(tmp_path):
    script_content = """\
somedcc.cmds.this_does_not_exists()
"""
    script_path = tmp_path / "startup_script.py"
    with open(script_path, "w") as fp:
        fp.write(script_content)

    os.environ["SOMEDCC_STARTUP_SCRIPT"] = str(script_path)
    os.environ.pop("SOMEDCC_SCRIPTS_PATH", None)

    with pytest.raises(StartupScriptError):
        SomeDCCGUI()


def test_scripts_path(tmp_path: Path):
    values = list(range(4))
    SOMEDCC_SCRIPTS_PATH = []
    for i in values:
        folder = tmp_path / f"script_dir_{i}"
        folder.mkdir()
        script = f"somedcc.set_data('TEST_{i}', {i})"
        script_dir_startup = folder / "startup.py"
        with open(script_dir_startup, "w") as fp:
            fp.write(f"\n{script}\n\n")
        SOMEDCC_SCRIPTS_PATH.append(str(folder))

    os.environ.pop("SOMEDCC_STARTUP_SCRIPT", None)
    os.environ["SOMEDCC_SCRIPTS_PATH"] = os.pathsep.join(SOMEDCC_SCRIPTS_PATH)

    somedcc = SomeDCCGUI()

    for i in values:
        assert somedcc.get_data(f"TEST_{i}") == i
