import pytest

from somedcc.sdk.app import App
from somedcc.sdk.cmd import Cmd
from somedcc.sdk.cmd import CmdGroup, RootCmdGroup


def test_app_build():
    class MakeCoffeeCmd(Cmd):
        def __call__(self, deca=False):
            self.deca = deca
            super().__call__(deca=deca)

        def do(self):
            print(f"making coffe ({self.deca})")

    class AddSugarCmd(Cmd):
        def __call__(self, nb=1):
            self.nb_sugar = nb
            super().__call__(nb=nb)

        def do(self):
            print(f"Adding {self.nb_sugar} sugar.")

    class PluginCmds(CmdGroup):
        make_coffee: MakeCoffeeCmd = Cmd.Bind()
        add_sugar: AddSugarCmd = Cmd.Bind()

    class Cmds(RootCmdGroup):
        plugin_cmds: PluginCmds = CmdGroup.Bind()

    class MyApp(App):
        cmds: Cmds = CmdGroup.Bind()

    app = MyApp()
    app.cmds.plugin_cmds.make_coffee(deca=True)
    app.cmds.plugin_cmds.add_sugar(3)


@pytest.fixture
def basic_app():
    class NotifyCmd(Cmd):
        def __call__(self, message) -> None:
            self._message = message
            return super().__call__(message)

        def do(self):
            self.app().notify(self._message)

    class QuitCmd(Cmd):
        def __call__(self) -> None:
            return super().__call__()

        def do(self):
            self.app().notify("He wants to quit you.")

    class AppCmds(CmdGroup):
        notify: NotifyCmd = Cmd.Bind()
        quit: QuitCmd = Cmd.Bind()

    class Cmds(RootCmdGroup):
        app: AppCmds = CmdGroup.Bind()

    class BasicApp(App):
        cmds: Cmds = CmdGroup.Bind()

    basic_app = BasicApp()
    basic_app.cmds.app.notify("BasicApp is ready.")
    return basic_app


def test_app_extension(basic_app: App):
    class _KVCmd(Cmd):
        def get_data(self):
            app: App = self.app()
            data = app.get_data("KV_STORE")
            if data is None:
                data = {}
                app.set_data("KV_STORE", data)
            return data

    class SetCmd(_KVCmd):
        def __call__(self, key, value) -> None:
            """
            Store the value under key.
            """
            self.key = key
            self.value = value
            return super().__call__(key=key, value=value)

        def do(self) -> None:
            self.get_data()[self.key] = self.value

    class GetCmd(_KVCmd):
        def __call__(self, key):
            """
            Retrieve the value stored under key.
            """
            self.key = key
            return super().__call__(key)

        def do(self) -> float:
            return self.get_data().get(self.key)

    class KVCmds(CmdGroup):
        get: GetCmd = Cmd.Bind()
        set: SetCmd = Cmd.Bind()

    # Extension cmd group are hand binded:
    type(basic_app.cmds).kv = KVCmds(basic_app.cmds, "kv")

    basic_app.cmds.kv.set("foo", "bar")
    foo = basic_app.cmds.kv.get("foo")
    assert foo == "bar"


def test_cmd_group_override(basic_app, capsys):
    with capsys.disabled():

        class ConnectCmd(Cmd):
            def __call__(self, url) -> None:
                self.url = url
                return super().__call__(url=url)

            def do(self):
                self.app().cmds.app.notify(f"Connecting to {self.url}")

        class MyAppGroup(type(basic_app.cmds.app)):
            connect: ConnectCmd = Cmd.Bind()

        bind = CmdGroup.Bind()
        bind.group_name = "app"
        bind.group_type = MyAppGroup
        type(basic_app.cmds).app = bind

        basic_app.cmds.app.connect("https://app.server.org")
        basic_app.cmds.app.notify("Does this still work ?")


def test_cmd_override(basic_app, capsys):
    with capsys.disabled():

        class MyQuitCmd(Cmd):
            def __call__(self) -> bool:
                return super().__call__()

            def do(self):
                self.app().cmds.app.notify("This is the custom cmd!")
                return True

        bind = Cmd.Bind()
        bind.cmd_name = "quit"
        bind.cmd_type = MyQuitCmd
        type(basic_app.cmds.app).quit = bind

        ret = basic_app.cmds.app.quit()
        assert ret is True
