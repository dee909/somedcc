import pytest

from somedcc.app.gui import SomeDCCGUI


@pytest.fixture
def somedcc() -> SomeDCCGUI:
    return SomeDCCGUI()


def test_cmds_usage(capsys):
    somedcc = SomeDCCGUI()

    with capsys.disabled():
        pass
    if 1:
        world_id = somedcc.cmds.items.create_root("World")
        group_id = somedcc.cmds.items.create(world_id, "GroupA")
        somedcc.cmds.items.create(group_id, "ItemA")

        somedcc.cmds.app.notify("TADAAA! All items created.")

        def cb():
            print("SAVING DOC ?")

        somedcc.cmds.gui.add_menu("Doc", "Save", cb)


def test_item_cmds(somedcc: SomeDCCGUI, capsys):
    with capsys.disabled():
        world_id = somedcc.cmds.items.create_root("world")
        group_id = somedcc.cmds.items.create(world_id, "Group")
        somedcc.cmds.items.create(group_id, "Item1")
        item2_id = somedcc.cmds.items.create(world_id, "Item2")

        new_item2_id = somedcc.cmds.items.reparent(item2_id, group_id)

        assert item2_id != new_item2_id

        value_item1 = somedcc.cmds.items.create(
            group_id, "Value1", "Value", dict(value=1)
        )
        value_item2 = somedcc.cmds.items.create(
            group_id, "Value2", "Value", {"value": 2}
        )

        value1 = somedcc.cmds.items.get_property(value_item1, "value")
        value2 = somedcc.cmds.items.get_property(value_item2, "value")

        assert value1 == 1
        assert value2 == 2


def test_to_script_cmd(somedcc: SomeDCCGUI, capsys):
    with capsys.disabled():
        world = somedcc.cmds.items.create_root("world")
        group = somedcc.cmds.items.create(world, "Main Group")
        somedcc.cmds.items.create(group, "Item A")
        somedcc.cmds.items.create(group, "Item B")

        script = somedcc.cmds.items.to_script(world)
        print(script)


def test_save_script_cmd(somedcc: SomeDCCGUI, tmp_path, capsys):
    with capsys.disabled():
        world = somedcc.cmds.items.create_root("world")
        group = somedcc.cmds.items.create(world, "Main Group")
        somedcc.cmds.items.create(group, "Item A")
        somedcc.cmds.items.create(group, "Item B")

        filename = tmp_path / "my_items_script.sdi"
        somedcc.cmds.items.save_script(world, filename)

        with open(filename, "r") as fp:
            print("====================================")
            print(fp.read())
            print("====================================")


def test_load_script_cmd(somedcc: SomeDCCGUI, tmp_path, capsys):
    with capsys.disabled():
        script_lines = [
            "somedcc.cmds.items.create_root(root_name='world')",
            "somedcc.cmds.items.create(parent_item_id='world', item_name='Main Group')",
            "somedcc.cmds.items.create(parent_item_id='world/Main Group', item_name='Item A')",  # noqa: ignore=E501
            "somedcc.cmds.items.create(parent_item_id='world/Main Group', item_name='Item B')",  # noqa: ignore=E501
        ]
        filename = tmp_path / "my_items_script.sdi"
        with open(filename, "w") as fp:
            fp.write("\n".join(script_lines))

        somedcc.cmds.items.load_script(filename)

        assert somedcc.cmds.items.get_root_names() == ["world"]

        assert somedcc.cmds.items.get_children_ids("world") == ["world/Main Group"]

        assert somedcc.cmds.items.get_children_ids("world/Main Group") == [
            "world/Main Group/Item A",
            "world/Main Group/Item B",
        ]
        assert somedcc.cmds.items.get_parent_id("world/Main Group") == "world"

        assert somedcc.cmds.items.get_children_ids("world/Main Group/Item A") == []
        parent_id = somedcc.cmds.items.get_parent_id("world/Main Group/Item A")
        assert parent_id == "world/Main Group"

        assert somedcc.cmds.items.get_children_ids("world/Main Group/Item B") == []
        parent_id = somedcc.cmds.items.get_parent_id("world/Main Group/Item B")
        assert parent_id == "world/Main Group"


def test_create_link_cmd(somedcc: SomeDCCGUI, capsys):
    with capsys.disabled():
        world = somedcc.cmds.items.create_root("world")
        LIB = somedcc.cmds.items.create(world, "LIB")

        foobar = somedcc.cmds.items.create(LIB, "FooBar")
        somedcc.cmds.items.create(foobar, "Foo")
        somedcc.cmds.items.create(foobar, "Bar")

        scene = somedcc.cmds.items.create(world, "Scene")
        foobar_1 = somedcc.cmds.items.create_link(scene, "FooBar 1", foobar)
        foobar_2 = somedcc.cmds.items.create_link(scene, "FooBar 2", foobar)

        print(somedcc.cmds.items.to_script(world))

        assert somedcc.cmds.items.get_children_ids(scene) == [
            scene + "/FooBar 1",
            scene + "/FooBar 2",
        ]

        assert somedcc.cmds.items.get_children_ids(
            foobar_1
        ) == somedcc.cmds.items.get_children_ids(foobar)

        assert somedcc.cmds.items.get_children_ids(
            foobar_2
        ) == somedcc.cmds.items.get_children_ids(foobar)
