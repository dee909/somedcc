from somedcc.sdk import pubsub
import pydantic


def test_event_creation(capsys):
    with capsys.disabled():

        class RangePayload(pubsub.Payload):
            first: int
            last: int

        payload = RangePayload(first=1, last=100)
        event = pubsub.Message(channel="test.channel", payload=payload)
        print(event)


def test_channel_matches():
    channel = "root.path.to.item_id.property"
    test_patterns = [
        (True, "root.path.to.item_id.property"),
        (False, "root.path.to.item_id.another_property"),
        #
        (True, "root.path.to.item_id.*"),
        (True, "root.path.to.item_id.prop*"),
        (False, "root.path.to.item_id.BLAH"),
        (False, "root.path.to.item_id.BLAH*"),
        #
        (True, "root.path.to.*.property"),
        (True, "root.path.to.>"),
        (False, "root.path.to.*"),
        #
        (True, "root.*.to.*.property"),
        (True, "*.*.*.*.property"),
        (True, "*.*.*.*.*"),
        (False, "*.*.*.*"),
        (False, "*.*.*.*.*.*"),
        #
        (True, "root.>.property"),
        (True, "root.*.>.property"),
        (True, "root.*.>"),
        (True, "root.*.>.*"),
        (True, ">.property"),
    ]
    for expected, patt in test_patterns:
        print("?", channel, patt)
        subscription = pubsub.Subscription(None, lambda: None, patt)
        assert subscription.interested(channel) == expected


def test_sdk_pubsub(capsys):  # noqa: ignore=C901
    with capsys.disabled():
        my_pupsub = pubsub.PubSub()

        class IntDB:
            class UpdatePayload(pubsub.Payload):
                ID: str
                old_value: int | None
                new_value: int

            def __init__(self, message_bus: pubsub.PubSub):
                self._data = {}
                self._pubsub = message_bus

            def set_value(self, ID: str, value: int):
                old_value = self.get_value(ID)
                self._data[ID] = value
                payload = self.UpdatePayload(
                    ID=ID, old_value=old_value, new_value=value
                )
                channel = "DB.updated." + ID
                self._pubsub.publish(payload, channel)

            def get_value(self, ID: str) -> int:
                return self._data.get(ID)

        class View:
            def __init__(self, name: str, db: IntDB, pubsub: pubsub.PubSub) -> None:
                self.name = name
                self._db = db
                self._ids = []
                pubsub.subscribe("DB.updated.*", self.on_updated)

            def add_ID(self, ID):
                self._ids.append(ID)
                self.update()

            def update(self):
                print(f"View {self.name} updating...")
                if 0:
                    for ID in self._ids:
                        v = self._db.get_value(ID)
                        print(f"    {ID}={v!r}")

            def on_updated(self, message: pubsub.Message):
                update: self._db.UpdatePayload = message.payload
                print(
                    f"Update: {update.ID}={update.new_value} ts:{message.timestamp} "
                    f"(was {update.old_value}) "
                )
                if update.old_value != update.new_value:
                    self.update()

        import random

        db = IntDB(my_pupsub)
        IDS = "abcdefgh"

        def set_random_value(ID):
            db.set_value(ID, random.randint(0, 100))

        def set_random_ID():
            ID = random.choice(IDS)
            set_random_value(ID)

        view1 = View("View1", db, my_pupsub)
        view2 = View("View2", db, my_pupsub)
        for ID in IDS:
            view1.add_ID(ID)
            view2.add_ID(ID)

        for i in range(100):
            print(".", end="")
            if not random.randint(0, 10):
                print("!")
                set_random_ID()
            my_pupsub.push()
