from somedcc.items.model import Root, Item, ValueItem


def test_item_properties(capsys):
    with capsys.disabled():
        props = dict(a=1, b=2)

        item = Item(None, "test", props)
        assert item.get("a") == 1
        assert item.get("b") == 2
        assert item.get("aa") is None
        assert item.get("aa", 11) == 11


def test_item_parenting(capsys):
    with capsys.disabled():
        root = Root("World")
        item_1 = Item(root, "Item 1", dict(value=1))
        sub_1 = Item(item_1, "Sub 1")

        assert item_1._parent == root
        assert sub_1._parent == item_1


def test_value_item(capsys):
    with capsys.disabled():
        root = Root("World")

        fps = ValueItem(root, "FPS", 24, dict(color="#880000"))

        assert fps.get("value") == 24
        assert fps.get("value", 30) == 24
        assert fps.value == 24

        fps.value = 60
        assert fps.get("value") == 60
        assert fps.get("value", 30) == 60
        assert fps.value == 60
