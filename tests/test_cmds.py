import pytest

from somedcc.sdk import cmd
from somedcc.sdk import app


@pytest.fixture
def test_app() -> app.App:
    class TestCmd(cmd.Cmd):
        @classmethod
        def can_undo(cls):
            # (This is needed to get the command
            # from undo history after execution.)
            return True

        def __call__(self, a: str, b: bool, c: int = 1):
            return super().__call__(a, b, c)

        def do(self):
            self.app().notify("It's a pigeon...")

    class Cmds(cmd.RootCmdGroup):
        test: TestCmd = cmd.Cmd.Bind()

    class TestApp(app.App):
        cmds: Cmds = cmd.CmdGroup.Bind()

    return TestApp()


def test_cmd_repr(test_app: app.App):
    test_app.cmds.test("Test", True, 2)
    cmd = test_app._undo_history[-1]
    assert repr(cmd) == "TestApp.cmds.test('Test', True, 2)"
