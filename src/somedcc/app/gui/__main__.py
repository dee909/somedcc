from . import SomeDCCGUI

from qtpy import QtWidgets
from .widgets.main_window import MainWindow


def main():
    somedcc = SomeDCCGUI()

    app = QtWidgets.QApplication()
    w = MainWindow(somedcc)
    w.show()
    print("This is SomeDCC - GUI mode.")
    app.exec_()


if __name__ == "__main__":
    main()
