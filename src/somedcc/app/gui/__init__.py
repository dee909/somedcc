from ...sdk.app import App
from ...sdk.cmd import RootCmdGroup, CmdGroup

from ..app_cmds import AppCmds
from .gui_cmds import GUICmds
from ...items.item_cmds import ItemsCmds


class SomeDCCGUICmds(RootCmdGroup):
    """
    All commands for SomeDCC GUI App.
    """

    app: AppCmds = CmdGroup.Bind()
    gui: GUICmds = CmdGroup.Bind()

    items: ItemsCmds = CmdGroup.Bind()


class SomeDCCGUI(App):
    """
    SomeDCC GUI App.
    """

    cmds: SomeDCCGUICmds = CmdGroup.Bind()

    @classmethod
    def app_name(cls):
        return "somedcc"

    @classmethod
    def has_gui(cls):
        return True

    # def __init__(self):
    #     # super().__init__("somedcc")
    #     # self.cmds = SomeDCCGUICmds("cmds", self)
