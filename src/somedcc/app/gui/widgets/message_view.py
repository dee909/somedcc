from qtpy import QtWidgets, QtCore

from ....sdk import pubsub
from .dockable_view import DockableView


class MessageView(DockableView):
    DEFAULT_AREA = QtCore.Qt.RightDockWidgetArea

    def _build(self, parent: QtWidgets.QWidget) -> None:
        self._unsubscribe = None

        self._filter_cb = QtWidgets.QComboBox(parent)
        self._filter_cb.setEditable(True)
        self._filter_cb.addItems(
            (
                ">",
                "app.>",
                "app.*",
            )
        )
        self._filter_cb.currentTextChanged.connect(self._on_filter_edited)
        self._on_filter_edited()

        self._te = QtWidgets.QTextEdit(parent)
        self._te.setLineWrapMode(self._te.NoWrap)

        b = QtWidgets.QPushButton(parent)
        b.setText("test me !")
        b.clicked.connect(self._test)

        parent.setLayout(QtWidgets.QVBoxLayout())
        parent.layout().addWidget(self._filter_cb)
        parent.layout().addWidget(self._te)
        parent.layout().addWidget(b)

    def _on_message(self, message: pubsub.Message):
        channel = message.channel
        # painfull combobox -__-
        texts = [self._filter_cb.itemText(i) for i in range(self._filter_cb.count())]
        if channel not in texts:
            self._filter_cb.addItem(channel)
        self._te.append(str(message))
        print(message)

    def _on_filter_edited(self):
        pattern = self._filter_cb.currentText().strip()
        if not pattern:
            pattern = ">"
        if self._unsubscribe is not None:
            self._unsubscribe()
        self._unsubscribe = self._view.subscribe(pattern, self._on_message)

    def _test(self):
        self._view.app.cmds.app.notify("test")
