from typing import Optional

from qtpy import QtWidgets, QtGui, QtCore

from ....sdk import view

from .message_view import MessageView


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, app):
        super().__init__(None)
        if not app.has_gui():
            raise ValueError("This App does not recognize me has a GUI! I'm offended.")
        self.setWindowTitle(app.app_name())

        self.resize(800, 800)

        self._main_toolbar = QtWidgets.QToolBar(self)
        self._main_toolbar.setWindowTitle("Main Toolbar")
        self.addToolBar(QtCore.Qt.LeftToolBarArea, self._main_toolbar)

        self._view = view.View(app)

        self.process_interval = 10  # in milliseconds
        self._on_process_interval()

        self._panel_types: list[tuple[QtWidgets.QWidget, str | None]] = []
        self._view_menu = QtWidgets.QMenu(self)
        self._view_toolbut = QtWidgets.QToolButton(self)
        self._view_toolbut.setText("Add View")
        self._view_toolbut.setPopupMode(self._view_toolbut.InstantPopup)
        self._view_toolbut.setMenu(self._view_menu)
        self._main_toolbar.addWidget(self._view_toolbut)

        self.register_panel_type(MessageView, "Messages")

    def _on_process_interval(self):
        self._view.app.process_messages()
        QtCore.QTimer.singleShot(self.process_interval, self._on_process_interval)

    def register_panel_type(self, panel_type, panel_name: Optional[str] = None):
        self._panel_types.append((panel_type, panel_name))
        self._update_view_menu()

    def _update_view_menu(self):
        self._view_menu.clear()
        for panel_type, panel_name in self._panel_types:
            panel_name = panel_name or panel_type.__name__
            self._view_menu.addAction(
                panel_name, lambda T=panel_type, n=panel_name: self.create_panel(T, n)
            )

    def create_panel(self, panel_type, panel_name):
        panel = panel_type(self._view.app, self)
        panel.setWindowTitle(panel_name)
