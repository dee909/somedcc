from qtpy import QtWidgets, QtGui, QtCore

from ....sdk import view


class DockableView(QtWidgets.QDockWidget):
    DEFAULT_AREA = QtCore.Qt.LeftDockWidgetArea

    def __init__(self, app, main_window):
        super().__init__(main_window)
        self._main_window = main_window
        self._view = view.View(app)

        main_window.addDockWidget(self.DEFAULT_AREA, self)
        self._widget = QtWidgets.QWidget(self)
        self.setWidget(self._widget)
        self._build(self._widget)

    def _build(self, parent):
        pass
