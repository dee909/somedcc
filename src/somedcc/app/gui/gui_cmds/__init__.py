from typing import Optional

from ....sdk.cmd import Cmd, CmdGroup

from ..widgets import main_window


class _GUICmd(Cmd):
    def main_window(self) -> main_window.MainWindow:
        return self.app().get_data("MAIN_WINDOW")


class RegisterPanelType(_GUICmd):
    def __call__(self, panel_type, panel_name: Optional[str] = None) -> None:
        self.panel_type = panel_type
        self.panel_name = panel_name
        return super().__call__(panel_type, panel_name)

    def do(self):
        self.main_window().register_panel_type(self.panel_type, self.panel_name)

    def undo(self):
        # TODO: test this...
        self.main_window().unregister_panel_type(self.panel_type)


class AddMenuAction(_GUICmd):
    def __call__(self, menu_name, action_name, action_callback):
        """
        Adds an action in a menu.
        """
        self.menu_name = menu_name
        self.action_name = action_name
        self.action_callback = action_callback
        super().__call__(menu_name, action_name, action_callback)

    def do(self):
        print("Adding an action to a menu in the gui")


class GUICmds(CmdGroup):
    """
    GUI related commands.
    """

    add_menu: AddMenuAction = CmdGroup.Bind()

    # def __init__(self, app):
    #     super().__init__(app)
    #     self.add_menu = AddMenuAction
