from ..sdk.cmd import Cmd, CmdGroup


class SetEchoAllCmdsCmd(Cmd):
    def is_query_only(self):
        return True

    def __call__(self, enabled: bool):
        self.enabled = enabled
        super().__call__(enabled)

    def do(self):
        self.app().set_echo_all_cmds(self.enabled)


class UndoCmd(Cmd):
    def __call__(self):
        """
        Just a joke ^^
        """
        super().__call__()

    def do(self):
        self.app().notify("Someone should implement `undo()`...")


class NotifyCmd(Cmd):
    def __call__(self, message: str):
        self.message = message
        super().__call__()

    def do(self):
        self.app().notify(self.message)


class AppCmds(CmdGroup):
    """
    Commands for Application stuff.
    """

    undo: UndoCmd = CmdGroup.Bind()
    notify: NotifyCmd = CmdGroup.Bind()
    set_echo_all_cmds: SetEchoAllCmdsCmd = CmdGroup.Bind()

    # def __init__(self, app):
    #     super().__init__(app)
    #     self.undo = UndoCmd
    #     self.notify = NotifyCmd
    #     self.set_echo_all_cmds = SetEchoAllCmdsCmd
