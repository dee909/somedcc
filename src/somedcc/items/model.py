from __future__ import annotations

from typing import Any, Union, Optional, Callable
from copy import deepcopy

import pydantic

from ..sdk.pubsub import Payload

JSONTypes = Union[str, int, float, bool, None]


class Item:
    # Special property names for modified events
    PROP_NAME = "_NAME_"
    PROP_CHILDREN = "_CHILDREN_"
    PROP_PARENT = "_PARENT_"

    def __init__(
        self, parent: Item, name: str, properties: Optional[dict[str, JSONTypes]] = None
    ):
        super().__init__()
        self._parent: Item = None
        self._name: str = name
        self._properties: dict[str, JSONTypes] = deepcopy(properties or {})
        self._children: list[Item] = []

        if parent is not None:
            self.reparent(parent)

    @property
    def name(self):
        # read only attribute
        return self._name

    @property
    def parent(self):
        # read only attribute
        return self._parent

    @property
    def children(self):
        # read only attribute
        return tuple(self._children)

    def get_persistent_children(self):
        """
        Some subclasses may have dynamically created children
        which you don't want to persist in scripts. They will
        override this to return a filtered list of children.
        """
        return self._children

    def reparent(self, new_parent: Item) -> str:
        """
        Change the item parent, returns the item new iid.
        """
        if self._parent is new_parent:
            return
        if self._parent is not None:
            self._parent._forget_child(self)
        new_parent._add_child(self)

    def _add_child(self, item: Item) -> None:
        self._children.append(item)
        item._parent = self
        self.root().on_item_modified(self, self.PROP_CHILDREN)

    def _forget_child(self, item: Item) -> None:
        self._children.remove(item)
        self.root().on_item_modified(self, self.PROP_CHILDREN)

    def item_id(self) -> str:
        return "/".join((self._parent.item_id(), self._name))

    def root(self):
        return self._parent.root()

    def get(self, property_name, *default):
        return self._properties.get(property_name, *default)

    def set(self, property_name, value):
        self._properties[property_name] = value
        self.root().on_item_modified(self, property_name)

    def get_item(self, sub_id):
        if not sub_id:
            return self

        if "/" in sub_id:
            child_name, sub_id = sub_id.split("/", 1)
        else:
            child_name, sub_id = sub_id, None

        if child_name == ".":
            return self.get_item(sub_id)

        if child_name == "..":
            return self._parent.get_item(sub_id)

        for item in self._children:
            if item.name == child_name:
                return item.get_item(sub_id)
        raise ValueError(
            f"Could not find child {child_name} under {self.item_id()} "
            f"(looking down for {sub_id}). "
            f" current children are: {self._children}"
        )


class ValueItem(Item):
    """
    A ValueItem is an item with a `value` property accessible
    as attribute (read and write)
    Don't ask...
    """

    def __init__(
        self,
        parent: Item,
        name: str,
        value: JSONTypes,
        properties: Optional[dict[str, JSONTypes]] = None,
    ):
        properties = properties or dict()
        properties["value"] = value
        super().__init__(parent, name, properties)

    @property
    def value(self):
        return self.get("value")

    @value.setter
    def value(self, new_value: JSONTypes):
        self.set("value", new_value)


class Root(Item):
    PROP_NEEDS_SAVING = "needs_saving"

    def __init__(self, name, properties: Optional[dict[str, JSONTypes]] = None):
        super().__init__(None, name, properties)
        self._needs_saving = True
        self._on_item_modified_callback = None

    def set_on_item_modified_callback(self, callback: Callable):
        """
        The callback signature: (modified_item:Item, property_name:Optional[str]=None)
        """
        self._on_item_modified_callback = callback

    def on_item_modified(self, item: Item, property_name: str = None):
        if (item, property_name) != (self, self.PROP_NEEDS_SAVING):
            self.set_needs_saving()
        if self._on_item_modified_callback is not None:
            self._on_item_modified_callback(item, property_name)

    def reparent(self):
        raise Exception("You can't reparent a root. Think about it...")

    def item_id(self):
        return self._name

    def root(self):
        return self

    def set_needs_saving(self):
        self.set(self.PROP_NEEDS_SAVING, True)

    def mark_saved(self):
        self.set(self.PROP_NEEDS_SAVING, False)


class LinkItem(Item):
    """
    The LinkItem lists the children of another item as if they were his.

    The real parent of those children must be specified in the 'source_item' property.
    """

    def __init__(
        self, parent: Item, name: str, properties: dict[str, JSONTypes] | None = None
    ):
        super().__init__(parent, name, properties)

    def get_source_item(self):
        return self.get("source_item", None)

    @property
    def children(self):
        source_item = self.get_source_item()
        if source_item is None:
            return []
        return source_item.children

    def get_persistent_children(self):
        return []
