from typing import Optional, Any
import os
import json

from ..sdk.cmd import Cmd, CmdGroup
from ..sdk import pubsub

from . import model

ItemIDType = str


class ItemCmd(Cmd):
    """
    Base class for Item commands.
    """

    APP_DATA_NAME = "ITEM_ROOTS"

    class ItemModifiedPayload(pubsub.Payload):
        item_id: ItemIDType
        property_name: str

    def on_item_modified(self, item: model.Item, property_name: Optional[str]):
        item_id = item.item_id()
        # TODO: shouldnt we have the root name in the channel to allow view to listen
        # to everything under a root ?
        channel = f"items.modified.{property_name}.{item_id}"
        payload = self.ItemModifiedPayload(item_id=item_id, property_name=property_name)
        self.app().pubsub.publish(payload, channel)

    def get_roots(self) -> list[model.Root]:
        return self.app().get_data(self.APP_DATA_NAME, [])

    def add_root(self, root: model.Root):
        item_roots = self.app().get_data(self.APP_DATA_NAME, [])
        item_roots.append(root)
        self.app().set_data(self.APP_DATA_NAME, item_roots)
        root.set_on_item_modified_callback(self.on_item_modified)

    def get_root_of(self, item_id: ItemIDType) -> model.Root:
        root_name = item_id.lstrip("/").split("/", 1)[0]
        for root in self.get_roots():
            if root.name == root_name:
                return root
        raise ValueError(f"Root {root_name} not found (for {item_id}).")

    def get_item(self, item_id: ItemIDType) -> model.Item:
        root = self.get_root_of(item_id)

        item_id = item_id.lstrip("/")
        if "/" not in item_id:
            # we look for a root:
            return root

        sub_id = item_id.split("/", 1)[-1]
        return root.get_item(sub_id)

    def serialize_properties(self, properties):
        serialized_properties = {}
        for k, v in properties.items():
            if isinstance(v, model.Item):
                v = "Item@" + v.item_id()
            serialized_properties[k] = v
        return serialized_properties

    def deserialize_properties(self, serialized_properties):
        if serialized_properties is None:
            return None
        properties = {}
        for k, v in serialized_properties.items():
            is_item_ref = False
            try:
                is_item_ref = v.startswith("Item@")
            except Exception:
                pass
            if is_item_ref:
                _, _, item_id = v.partition("Item@")
                item = self.get_item(item_id)
                v = item
            properties[k] = v
        return properties


class ItemQueryCmd(ItemCmd):
    def is_query_only(self):
        return True


class CreateItemCmd(ItemCmd):
    def __call__(
        self,
        parent_item_id: ItemIDType,
        item_name: str,
        item_type_name: Optional[str] = None,
        properties: Optional[dict[str, Any]] = None,
    ):
        """
        Create an new Item of type `item_type_name` with name `item_name`,
        optionnally parented to item `parent_item`.

        Available item types:
            `None|Item` : An item with name and properties
            `Value`     : An item with name, value and properties
                            Note: you must provide the value in the 'value' key
                            of the properties argument.

        Returns the `id` (str) of the created item.
        """
        self.parent_item_id = parent_item_id
        self.item_name = item_name
        self.item_type_name = item_type_name
        self.properties = properties
        return super().__call__(parent_item_id, item_name, item_type_name, properties)

    def do(self):
        print("creating item", self.item_type_name, self.item_name, self.parent_item_id)
        properties = self.deserialize_properties(self.properties)
        parent = self.get_item(self.parent_item_id)
        if not self.item_type_name or self.item_type_name == "Item":
            item = model.Item(parent, self.item_name, self.properties)
        elif self.item_type_name == "Value":
            value = properties["value"]
            item = model.ValueItem(parent, self.item_name, value, properties=properties)
        elif self.item_type_name == "Link":
            item = model.LinkItem(parent, self.item_name, properties=properties)
        else:
            raise ValueError(f"Unknow item type {self.item_type_name}")

        return item.item_id()


class CreateRootCmd(ItemCmd):
    def __call__(
        self,
        root_name: str,
        properties: Optional[dict[str, Any]] = None,
    ):
        """Create a root item with the given name.

        Args:
            name (str): name of the root item.
            properties (dict(str, any)): optional properties for the root item.
        """
        self.root_name = root_name
        self.properties = properties
        return super().__call__(root_name, properties)

    def do(self):
        print("Creating Root", self.root_name)
        properties = self.deserialize_properties(self.properties)
        root = model.Root(self.root_name, properties)
        self.add_root(root)
        return root.item_id()


class CreateLinkCmd(ItemCmd):
    def __call__(
        self, parent_item_id: ItemIDType, item_name: str, source_item_id: ItemIDType
    ):
        self.parent_item_id = parent_item_id
        self.item_name = item_name
        self.source_item_id = source_item_id
        return super().__call__(parent_item_id, item_name, source_item_id)

    def do(self):
        parent = self.get_item(self.parent_item_id)
        source_item = self.get_item(self.source_item_id)
        properties = {"source_item": source_item}
        link = model.LinkItem(parent, self.item_name, properties)
        return link.item_id()


class GetRootNamesCmd(ItemQueryCmd):
    def do(self):
        roots = self.get_roots()
        return [root.name for root in roots]


class GetParentIdCmd(ItemQueryCmd):
    def __call__(self, item_id):
        self.item_id = item_id
        return super().__call__(item_id)

    def do(self):
        item = self.get_item(self.item_id)
        parent = item.parent
        if parent is None:
            return None
        return parent.item_id()


class GetChildrenIdsCmd(ItemQueryCmd):
    def __call__(self, item_id):
        self.item_id = item_id
        return super().__call__(item_id)

    def do(self):
        item = self.get_item(self.item_id)
        return [child.item_id() for child in item.children]


class ReparentCmd(ItemCmd):
    def __call__(self, item_id: ItemIDType, new_parent_id: ItemIDType):
        self.item_id = item_id
        self.new_parent_id = new_parent_id
        return super().__call__(item_id, new_parent_id)

    def do(self):
        print("Reparenting", self.item_id, "to", self.new_parent_id)
        item = self.get_item(self.item_id)
        new_parent = self.get_item(self.new_parent_id)
        item.reparent(new_parent)


class GetPropertyCmd(ItemQueryCmd):
    def __call__(
        self, item_id: ItemIDType, property_name: str, default: Optional[Any] = ...
    ):
        self.item_id = item_id
        self.property_name = property_name
        self.default = default
        return super().__call__(item_id, property_name, default)

    def do(self):
        item = self.get_item(self.item_id)
        default = ()
        if self.default is not ...:
            default = (self.default,)
        return item.get(self.property_name, *default)


class SetPropertyCmd(ItemCmd):
    def __call__(self, item_id: ItemIDType, property_name: str, value: Any):
        """Change the value of an Item's property.

        Args:
            item_id (ItemIDType): ID of the Item to affect
            property_name (str): name of the property to affect
            value (Any): new value (make it json serializable, pplease.)

        Return:
            None
        """
        self.item_id = item_id
        self.property_name = property_name
        self.value = value
        return super().__call__(item_id, property_name, value)

    def do(self):
        item = self.get_item(self.item_id)
        item.set(property, self.value)


class ToScriptCmd(ItemCmd):
    def __call__(self, item_id):
        self.item_id = item_id
        return super().__call__(item_id)

    def do(self):
        script_lines = []
        item = self.get_item(self.item_id)
        self.get_item_script(script_lines, item)
        return "\n".join(script_lines)

    def get_item_script(self, script_lines, item):
        kwargs = []
        name = item.name

        if isinstance(item, model.Root):
            # cmd_name = CreateRootCmd.cmd_path()
            cmd_name = self.app().cmds.items.create_root.cmd_path()
            kwargs.append(["root_name", name])

        else:
            # FIXME: choose this:
            # We use to do this:
            #       parent_item_id = GetParentIdCmd(item.item_id())
            # But with the Cmd.Bind system, we can't anymore.
            if 0:
                # One option is:
                parent_item_id = self.app().cmds.items.get_parent_id(item.item_id())
                # But I needs to know all the groups leading to the cmd :/
            else:
                # But since it's a querry cmd, another option is:
                cmd = GetParentIdCmd(self.app().cmds, "get_parent_id")
                parent_item_id = cmd(item.item_id())
                # But it's uggly and hard to explain
                # (and logged cmd will be wrong if echo_all_cmds is set on the app)

            kwargs.append(["parent_item_id", parent_item_id])
            kwargs.append(["item_name", name])
            # cmd_name = CreateItemCmd.cmd_path()
            cmd_name = self.app().cmds.items.create.cmd_path()

            type_name = type(item).__name__
            if type_name != "Item":
                kwargs.append(["item_type_name", type_name])

        properties = item._properties  # FIXME: use a cmd to get all the properties ?
        if properties:
            properties = self.serialize_properties(properties)
            kwargs.append(["properties", properties])

        line = cmd_name + "(" + ", ".join([f"{k}={v!r}" for k, v in kwargs]) + ")"
        script_lines.append(line)
        for child in item.get_persistent_children():
            self.get_item_script(script_lines, child)


class SaveScriptCmd(ItemCmd):
    def __call__(
        self,
        root_id: Optional[ItemIDType] = None,
        filename: Optional[str] = None,
        force: Optional[bool] = False,
    ):
        self.root_id = root_id
        self.filename = filename
        self.force = force
        return super().__call__(root_id, filename, force)

    def do(self):
        root: model.Root = self.get_item(self.root_id)
        filename = self.filename
        if filename is None:
            filename = root.get("filename")
        if filename is None:
            raise ValueError(
                "Could not save without a filename (Not, given in arguments, "
                "and not found in root properties)."
            )
        if os.path.exists(filename) and not self.force:
            raise ValueError(
                f"File {filename} already exists. Use `force` to override."
            )
        # FIXME: easier way to use a cmd inside another cmd:
        script = self.app().cmds.items.to_script(root.item_id())
        with open(filename, "w") as fp:
            fp.write(script + "\n")

        root.mark_saved()


class LoadScriptCmd(ItemCmd):
    def __call__(self, filename):
        self.filename = filename
        return super().__call__(filename)

    def do(self):
        with open(self.filename, "r") as fp:
            script_content = fp.read()

        exec(script_content, dict(somedcc=self.app()))


class ItemsCmds(CmdGroup):
    create: CreateItemCmd = Cmd.Bind()
    create_root: CreateRootCmd = Cmd.Bind()
    create_link: CreateLinkCmd = Cmd.Bind()
    get_root_names: GetRootNamesCmd = Cmd.Bind()
    get_parent_id: GetParentIdCmd = Cmd.Bind()
    get_children_ids: GetChildrenIdsCmd = Cmd.Bind()
    reparent: ReparentCmd = Cmd.Bind()
    get_property: GetPropertyCmd = Cmd.Bind()
    to_script: ToScriptCmd = Cmd.Bind()
    save_script: SaveScriptCmd = Cmd.Bind()
    load_script: LoadScriptCmd = Cmd.Bind()
