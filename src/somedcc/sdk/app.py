from typing import Optional
from pathlib import Path
import os
import sys

from . import pubsub

import pydantic


class StartupScriptError(Exception):
    pass


class AppSettings(pydantic.BaseSettings):
    STARTUP_SCRIPT: Optional[Path] = None
    SCRIPTS_PATH: Optional[str] = None

    class Config:
        env_prefix = "SOMEDCC_"


class App:
    """
    The App contains data and commands.
    It can emit and dispatch events to subcribers.
    """

    class NotifyPayload(pubsub.Payload):
        message: str

    class CmdLogPayload(pubsub.Payload):
        cmd: str

    @classmethod
    def app_name(cls):
        """
        Subclasses may override to specify an alternative
        name for the app.

        Default is to use the class name.

        BEWARE: this name will be used for scripting ans should be
        a valid python identifier !
        """
        return cls.__name__

    @classmethod
    def has_gui(cls):
        return False

    def __init__(self):
        super().__init__()

        self._settings = AppSettings()

        self._pubsub = pubsub.PubSub()

        self._data = {}

        self._echo_all_cmds = False
        self._undo_history = []

        self._startup()

    @property
    def pubsub(self) -> pubsub.PubSub:
        return self._pubsub

    def process_messages(self):
        self.pubsub.push()

    def notify(self, message: str):
        payload = self.NotifyPayload(message=message)
        self.pubsub.publish(payload, "app.notified")
        print(f"[{self.app_name()}]", message)

    def set_echo_all_cmds(self, enabled: bool):
        self._echo_all_cmds = enabled

    def echo_all_cmds(self):
        return self._echo_all_cmds

    def execute_cmd(self, cmd):
        log_payload = self.CmdLogPayload(cmd=str(cmd))
        if not cmd.is_query_cmd():
            # FIXME: un-undoable cmds should trigger an hitory clear
            if cmd.can_undo():
                self._undo_history.append(cmd)
            self.pubsub.publish(log_payload, "cmd.log")
        elif self.echo_all_cmds():
            self.notify(str(cmd))
            self.pubsub.publish(log_payload, "cmd.log")
        return cmd.do()

    def get_data(self, data_name, *default):
        return self._data.get(data_name, *default)

    def set_data(self, data_name, data):
        self._data[data_name] = data

    def _startup(self):
        startup_script = self._settings.STARTUP_SCRIPT
        if startup_script is not None:
            if not startup_script.exists():
                raise StartupScriptError(
                    f"Startup script does not exists: {startup_script}"
                )
            try:
                self._run_script(startup_script)
            except Exception as err:
                raise StartupScriptError(err)

        scripts_path_str = self._settings.SCRIPTS_PATH
        if scripts_path_str is not None:
            scripts_path = scripts_path_str.split(os.pathsep)
            for path in scripts_path:
                if os.path.isdir(path):
                    # TODO: shouldn't we add paths even when they don't exist ?
                    # (it would be easier to debug stuff)
                    sys.path.append(path)
                    script = os.path.join(path, "startup.py")
                    if os.path.exists(script):
                        self._run_script(script)

    def _run_script(self, script: Path):
        self.notify(f'Executing Script "{script}"...')
        ctx = dict(__file__=script)
        ctx[self.app_name()] = self
        with open(script) as fp:
            content = fp.read()
            exec(content, ctx, ctx)
