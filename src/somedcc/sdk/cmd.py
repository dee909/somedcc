"""



"""
from __future__ import annotations

from typing import Type, get_type_hints
import abc

from .app import App


class Cmd(abc.ABC):
    """
    Subclass this to create a new Command, then
    register the command in a CmdGroup like this:

    class MyCmdGroup(CmdGroup):
        my_cmd: MyCmdSubclass = Cmd.Bind()
    """

    class Bind:
        """
        Binds a Cmd type in a CmgGroup.
        """

        def __init__(self) -> None:
            self.cmd_name = None
            self.cmd_type = None

        def __set_name__(self, owner_class, name):
            self.cmd_name = name
            self.cmd_type = get_type_hints(owner_class).get(self.cmd_name)
            if self.cmd_type is None:
                raise TypeError(
                    "Unspecified type (annotation) for cmd "
                    f"{self.cmd_name} in {owner_class}"
                )

        def __get__(self, owner, owner_class=None):
            cmd = self.cmd_type(owner, self.cmd_name)
            return cmd

    @classmethod
    def is_query_cmd(self):
        """
        Subclasses must override this to return True
        if the cmd does not affect any state.
        """
        return False

    @classmethod
    def can_undo(self):
        """
        Subclasses must override this to return True
        if the cmd implements `self.undo()`.
        """
        return False

    def __init__(self, cmd_group, cmd_name) -> None:
        super().__init__()
        self._cmd_group = cmd_group
        self._cmd_name = cmd_name
        self._call_args_repr = "!!!"

    def cmd_path(self):
        return f"{self._cmd_group._cmd_path()}.{self._cmd_name}"

    def __repr__(self):
        return f"{self.cmd_path()}({self._call_args_repr})"

    def app(self):
        return self._cmd_group._app()

    def _make_call_args_repr(self, *args, **kwargs):
        """
        This is called while executing the cmd to build
        the repr of the calling args (passed to this call).

        Subclasses may override to alter default behavior.
        """
        args = [repr(arg) for arg in args]
        args += [f"{k}={v!r}" for k, v in kwargs.items()]
        return ", ".join(args)

    def __call__(self, *args, **kwargs) -> None:
        """
        Subclasses must overide this to set the cmd signature,
        store the arguments needed for `self.do()` self.redo()`
        and `self.undo()`, and call the base implementation with all
        arguments passed here.

        Subclasses must also annotate the return value according to
        `self.do()`.
        """
        self._call_args_repr = self._make_call_args_repr(*args, **kwargs)
        return self.app().execute_cmd(self)

    @abc.abstractmethod
    def do(self) -> None:
        ...

    def redo(self) -> None:
        """
        Calls `self.do()`
        Subclasses my override to implement a smarter behavior.
        """
        return self.do()

    def undo(self):
        """
        Suclasses must implement this and override `can_undo()`
        to return True in order to support undo.

            Note: Executing a cmd which does not support undo
            will clear the cmd history and prevent any further undo.

        """
        raise NotImplementedError


class CmdGroup:
    class Bind:
        """
        A descriptor to bind a CmdGroup in a paretn group:

        class MyCmdGroup(CmdGroup):
            sub_group : MyCmdSubGroup = CmdGroup.Bind()

        """

        def __init__(self) -> None:
            self.group_name = None
            self.group_type = None

        def __set_name__(self, owner_class, name):
            self.group_name = name
            self.group_type = get_type_hints(owner_class).get(self.group_name)
            if self.group_type is None:
                raise TypeError(
                    f"Unspecified type (annotation) for group {self.group_name} "
                    f"in {self.parent_group_type}"
                )

        def __get__(self, parent_group, parent_group_type=None):
            cmd_group = self.group_type(parent_group, self.group_name)
            return cmd_group

    def __init__(self, parent_group: CmdGroup, group_name: str) -> None:
        self._parent_group = parent_group
        self._group_name = group_name

    def _root_cmd_group(self):
        return self._parent_group._root_cmd_group()

    def _app(self) -> App:
        return self._root_cmd_group()._app()

    def _cmd_path(self) -> str:
        return self._parent_group._cmd_path() + "." + self._group_name


class RootCmdGroup(CmdGroup):
    def __init__(self, app: App, group_name: str) -> None:
        # Note: __init__ args are compatible with CmdGroup.Bind()
        # and it's crutial for building app's root cmd group.
        super().__init__(None, group_name)
        # this is private bc many app will want to define a cmd group
        # names "app" and it would clash.
        self.__app = app

    def _root_cmd_group(self) -> CmdGroup:
        return self

    def _cmd_path(self) -> str:
        return self._app().app_name() + "." + self._group_name

    def _app(self) -> App:
        return self.__app
