from typing import Callable

from ..sdk import pubsub, app


class View:
    def __init__(self, app: app.App) -> None:
        self.app = app

    def subscribe(self, channel_pattern: str, callback: Callable) -> callable:
        """
        Will call `callback` for each message in all channels matching
        `channel_pattern`.

        `channel_pattern` may use "*" and ">".
        See somedcc.sdk.pubsub.Subcription for mare details.

        Return a callable which will unsubcribe.
        """
        return self.app.pubsub.subscribe(channel_pattern, callback)

    # def publish(self, payload: pubsub.Payload, channel):
    #     self.app.pubsub.publish(payload, channel)
