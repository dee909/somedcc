from __future__ import annotations

from typing import Callable, Any, Optional
import re
import getpass
import time

import pydantic


class Payload(pydantic.BaseModel):
    pass


class Message(pydantic.BaseModel):
    channel: str
    user: str = "???"
    timestamp: float = 0.0
    saga: Optional[str] = None
    payload: Optional[Payload] = None

    class Config:
        validate_assignment = True

    @pydantic.root_validator
    def auto_fields(cls, values):
        values["user"] = getpass.getuser()
        values["timestamp"] = time.time()
        return values


class PubSub:
    def __init__(self) -> None:
        self._subscriptions: list[Subscription] = []
        self._message_queue: list[tuple[str, Message]] = []

    def subscribe(self, channel_pat: str, callback: Callable) -> callable:
        sub = Subscription(self, callback, channel_pat)
        self._subscriptions.append(sub)
        return sub.unsubscribe

    def unsubscribe(self, subscription: Subscription):
        self._subscriptions.remove(subscription)

    def publish(self, payload: Payload, channel: str):
        message = Message(channel=channel, payload=payload)
        self._message_queue.append((channel, message))

    def queue_len(self):
        return len(self._message_queue)

    def push(self, max_messages: Optional[int] = None):
        nb = self.queue_len()
        if max_messages:
            nb = min(max_messages, nb)
        for i in range(nb):
            channel, message = self._message_queue.pop(0)
            for subscription in self._subscriptions:
                if subscription.interested(channel):
                    subscription.receive(message)


class Publisher:
    def __init__(self, pubsub: PubSub) -> None:
        self.pubsub = pubsub

    def publish(self, channel, message):
        self.pubsub.publish(channel, message)


class Subscription:
    @classmethod
    def channel_pattern_to_re_pattern(cls, channel_pat: str):
        """
        A channel name contains colon separated parts.
        The channel pattern can use:
            "*" to match a whole part of the channel name.
            ">" to match all parts from here.

        Returns a compiled re pattern which will only match the
        channel it should.
        """
        # escape re special chars, except the "*":
        chunks = [re.escape(chunk) for chunk in channel_pat.split("*")]
        pattern = "*".join(chunks)

        # "*" matches until next "."
        pattern = pattern.replace("*", r"[^\.]+")
        # ">" matches anything
        pattern = pattern.replace(">", ".+")
        # The pattern must match the whole channel name
        pattern = f"^{pattern}$"

        return re.compile(pattern)

    @classmethod
    def re_pattern_matches_channel(cls, re_pat, channel):
        ret = re_pat.match(channel)
        return ret is not None

    def __init__(self, pubsub: PubSub, callback: Callable, channel_pat) -> None:
        self.pubsub = pubsub
        self.callback = callback
        self.channel_pat = channel_pat
        self.re_pat = self.channel_pattern_to_re_pattern(channel_pat)

    def unsubscribe(self):
        self.pubsub.unsubscribe(self)

    def interested(self, channel):
        """
        Returns True if this Subscription should receive message
        from the given channel.
        """
        return self.re_pattern_matches_channel(self.re_pat, channel)

    def receive(self, message: Message):
        self.callback(message)
